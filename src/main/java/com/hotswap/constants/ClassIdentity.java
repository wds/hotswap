package com.hotswap.constants;

public class ClassIdentity {

    public interface Suffix {
        String CLASS = ".class";

        String SOURCE = ".java";
    }

    public interface Type {
        String CLASS = "class";

        String SOURCE = "source";
    }

}
