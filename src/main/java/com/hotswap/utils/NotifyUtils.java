package com.hotswap.utils;

import com.intellij.notification.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;

public class NotifyUtils {
    private static final NotificationGroup NOTIFICATION = new NotificationGroup("hotswap", NotificationDisplayType.BALLOON, false);

    public static void notifyMessage(Project project, String message) {
        try {
            Notification currentNotify = NOTIFICATION.createNotification("hotswap", message, NotificationType.INFORMATION, null);
            Notifications.Bus.notify(currentNotify, project);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void notifyMessage(Project project, String message, NotificationType type) {
        try {
            Notification currentNotify = NOTIFICATION.createNotification("hotswap", message, type, null);
            Notifications.Bus.notify(currentNotify, project);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void error(Project project, String errorMsg) {
        Messages.showMessageDialog(project, errorMsg,
                " Hot Swap", Messages.getErrorIcon());
    }
}
