package com.hotswap.utils;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class IoUtils {

    public static String getResourceFile(ClassLoader classLoader, String filePath) throws Exception {
        try (InputStream in = classLoader.getResourceAsStream(filePath)) {
            if (in == null) {
                throw new IOException(filePath + " can not be found ");
            }
            InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;
            StringBuilder builder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line).append("\n");
            }
            return builder.toString();
        }
    }

    public static String printStackTrace(Throwable t) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        t.printStackTrace(printStream);
        try {
            printStream.close();
            byteArrayOutputStream.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return new String(byteArrayOutputStream.toByteArray());
    }
}
