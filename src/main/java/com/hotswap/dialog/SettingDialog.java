package com.hotswap.dialog;

import com.hotswap.domain.AppSettingsState;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class SettingDialog implements Configurable {
    private AppSettingsComponent mySettingsComponent;
    private Project project;
    /**
     * 设置信息
     */
    private AppSettingsState settings;

    public SettingDialog(Project project) {
        this.project = project;
        settings = AppSettingsState.getInstance(this.project);
    }

    @Nls(capitalization = Nls.Capitalization.Title)
    @Override
    public String getDisplayName() {
        return "HotSwap";
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        mySettingsComponent = new AppSettingsComponent();
        return mySettingsComponent.getPanel();
    }

    @Override
    public boolean isModified() {
        return !mySettingsComponent.getHostsText().equals(settings.hosts);
    }

    @Override
    public void apply() throws ConfigurationException {
//        if (StringUtils.isNotBlank(mySettingsComponent.getOssEndpointText())
//            && !mySettingsComponent.getOssEndpointText().contains(CommonConstants.URL_SEPARATOR)) {
//            throw new ConfigurationException("endpoint should start with http:// or https://");
//        }
        settings.hosts = mySettingsComponent.getHostsText();
    }

    @Override
    public void reset() {
        mySettingsComponent.setHostsText(settings.hosts);
    }

    @Override
    public void disposeUIResources() {
        mySettingsComponent = null;
    }
}
